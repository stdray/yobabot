﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Xml;

using NLog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

using agsXMPP;
using agsXMPP.protocol.x.muc;
using agsXMPP.protocol.client;
using YobaBot.App;


namespace YobaBot.Bot {

  public class ChatBot {

    _username           : string;
    _domain             : string;
    _pwd                : string;
    _nick               : string;
    _room               : string;
    _server             : string;
    _roomJid            : Jid;
    _timer              : Timer;
    mutable _run                : bool;
    mutable _connection : XmppClientConnection;
    mutable _conference : MucManager;

    public BotNick      : string  { get { _nick } }
    public RoomJid      : Jid { get { _roomJid }}
    public Connection   : XmppClientConnection { get { _connection } }
    public Conference   : MucManager { get { _conference } }

    public NoteRep      : INoteRepository;
    public TwitRep      : ITwitterRepository;
    public ProfRep      : IProfileRepository;
    public UserRep      : IUserRepository;
    public JuickRep     : IJuickRepository;

    public event OnMessage     : EventHandler[MsgInfo];
    public event OnPresence    : EventHandler[PresenceInfo];
    public event OnError       : EventHandler[Exception];
    public event OnIq          : EventHandler[IqInfo];
    public event OnStateChange : EventHandler[XmppConnectionState];

    Logger : Logger = LogManager.GetCurrentClassLogger();

    public this(conInfo : ConInfo, roomInfo : RoomInfo, repInfo : RepCtors) {
      _username   = conInfo.username;
      _domain     = conInfo.domain;
      _pwd        = conInfo.pwd;
      _nick       = roomInfo.nick;
      _room       = roomInfo.room;
      _server     = roomInfo.server;
      _roomJid    = Jid(_room, _server, null);
      NoteRep  = repInfo.NoteRep(this);
      TwitRep  = repInfo.TwitRep(this);
      ProfRep  = repInfo.ProfRep(this);
      UserRep  = repInfo.UserRep(this);
      JuickRep = repInfo.JuickRep(this);
      _timer = Timer() <- 
      {
        Enabled  = false;
        Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
        Elapsed += tryConnect;
      }
    }
    makeConnection() : XmppClientConnection
    {
      Logger.Info("Create new connection");
      XmppClientConnection() <- 
      {
        Username                     = _username;
        Server                       = _domain;
        Password                     = _pwd;
        Priority                     = 0; 
        KeepAlive                    = true;
        OnXmppConnectionStateChanged += (_, s) => connectionStateChanged(s);
        OnLogin                      += onLogin;
        OnMessage                    += onMessage;
        Resource                     = null;
        OnIq                         += onIq;
        OnPresence                   += onPresence;
      }
    }
    
    onIq(_ : object, iq : IQ) : void 
    {
      invokeHandler(OnIq, IqInfo(this, iq))
    }
    
    onPresence(_ : object, presence : Presence) : void 
    {
      invokeHandler(OnPresence, PresenceInfo(this, presence))
    }
    
    onMessage(_ : object, msg : Message) : void 
    {
      Logger.Info("{0} -> {1} : {2}", msg.From, msg.To, msg.Body);
      def userInfo  = UserRep.GetUserInfo(msg.From.ToString());
      invokeHandler(OnMessage, MsgInfo(this, msg, userInfo));
    }
    
    onLogin(_: object) : void 
    {
      def e = $<# <presence to = "$_room@$_server/$_nick">
                         <x xmlns='http://jabber.org/protocol/muc'>
                             <history maxstanzas= '0' />
                         </x>
                     </presence> #>;
      _conference = MucManager(_connection);
      _connection.Send(e);
      _conference.JoinRoom(_roomJid, _nick);
      _connection.Send(e);
    }
    
    tryConnect(_ : object, _ : ElapsedEventArgs) : void 
    {
      match(_connection.XmppConnectionState) {
      | XmppConnectionState.SessionStarted =>  
        Logger.Info($"$(_connection.XmppConnectionState), timer stopped");
        _timer.Stop();  
      | _ => 
        try
        {
          Logger.Info($"$(_connection.XmppConnectionState), coonecting recreating, timer active");
          _connection.Close(); 
          _connection = makeConnection();
          _connection.Open();
        }
        catch
        {
          | ex => Logger.ErrorException(_connection.XmppConnectionState.ToString(), ex)
        }
      }
    }
    
    reconnect() : void 
    {
      when(_run)
      {
        Logger.Info("Reconnect requested");
        when(_connection == null)
        {
          _connection = makeConnection();
          _connection.Open();
        }
        _timer.Start();
      }
    }
    
    connectionStateChanged(state : XmppConnectionState) : void 
    {
      Logger.Info("Connection state changed to: {0}", state);
      match(state) 
      {
        | XmppConnectionState.Disconnected   => reconnect();
        | _                                  => ();
      }
    }

    public Connect() : void 
    { 
      _run = true;
      Logger.Info("Connect");
      reconnect() 
    }
    
    public Disconect() : void
    { 
      _run = false;
      Logger.Info("Disconect");
      _conference?.LeaveRoom(_roomJid, _nick);
      _connection.Close();
      _timer.Stop();
      _connection = null;
    }
    
    public Send(text : string, msgType : MessageType) : void 
    {
      _connection.Send(Message(_roomJid, msgType, text));
    }

    public SendTo(to : string , text : string) : void 
    {
      _connection.Send(Message(to, text));
    }

    public Response(srcMsg: Message, text : string) : void 
    {
      def response = match(srcMsg.Type) 
      {
        | MessageType.groupchat => Message(_roomJid, srcMsg.Type , text);
        | _                     => Message(srcMsg.From, srcMsg.Type , text);
      }
      _connection.Send(response);
    }

    invokeHandler[T](handler : EventHandler[T], data : T) : void 
    {
      when(handler != null) 
        _ = Task.Run(() => handler.Invoke(this, data))
    }
  }
}