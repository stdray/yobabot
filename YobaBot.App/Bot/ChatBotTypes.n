﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

using agsXMPP;
using agsXMPP.protocol.x.muc;
using agsXMPP.protocol.client;
using YobaBot.App;

namespace YobaBot.Bot {

    type RoleItem = agsXMPP.protocol.x.muc.iq.admin.Item;
    type ChatRole = agsXMPP.protocol.x.muc.Affiliation;
    
    public variant BotAnswer {
        | Default
        | Nothing
        | Text { text : string }
        public override ToString() : string {
            match(this){
                | Default => "ok"
                | Text(t) => t
                | Nothing => string.Empty
            }
        }
    }
    
    public variant UserType {
        | Owner
        | Admin
        | Moderator
        | Member
        | Voice
        | Ban
    }

    [Record] public class ConInfo {
        public username   : string; 
        public domain     : string; 
        public pwd        : string;
        public this(jid : string, pwd : string) {
            def (username, domain) = match(jid.Split(array['@']).NToList()){
                | [u, d] => (u, d)
                | _      => throw ArgumentException("Некорректный ЖИД");
            }
            this(username, domain, pwd);
        }
    }
    
    [Record] public class RepCtors {
        public UserRep  : ChatBot -> IUserRepository;
        public NoteRep  : ChatBot -> INoteRepository;
        public TwitRep  : ChatBot -> ITwitterRepository;
        public ProfRep  : ChatBot -> IProfileRepository;
        public JuickRep : ChatBot -> IJuickRepository;
    }
    
    [Record] public class RoomInfo {
        public nick      : string;
        public room      : string; 
        public server    : string;
    }

    [Record] public class BotData[T] {
        public bot  : ChatBot;
        public data : T;
    }
    
    [Record] public class MsgInfo : BotData[Message]{
        public From : option[UserInfo];
    }
    type PresenceInfo = BotData[Presence];
    type IqInfo       = BotData[IQ];
    type ConStateInfo = BotData[XmppConnectionState];
}
