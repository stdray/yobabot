﻿using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Console;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Topshelf;
using YobaBot.Bot;
using YobaBot.App;
using Newtonsoft.Json;

module Program 
{
  Main() : void 
  {
    _ = HostFactory.Run(x =>
    {
      _ = x.Service(s => 
      {
        s.ConstructUsing(_ => Yoba());
        _ = s.WhenStarted(y : Yoba => y.Start());
        _ = s.WhenStopped(y : Yoba => y.Stop());
      });
      _ = x.RunAsPrompt();
      _ = x.StartAutomatically();
          x.SetDescription("Yoba chat bot");
          x.SetDisplayName("Yoba");
          x.SetServiceName("Yoba");
      _ = x.EnableServiceRecovery(r =>
      {
        _ = r.RestartService(1);
            r.OnCrashOnly();
        _ = r.SetResetPeriod(1);
      });
      _ = x.UseNLog();
    });
  }
}

class Yoba
{
  private static s : Settings = Settings.Default;
  bot : ChatBot;
  
  public this()
  {
    def conInfo  = ConInfo(s.BotJid, s.BotPwd);
    def roomInfo = RoomInfo(s.BotNick,  s.ChatRoom, s.ChatServ);
    def getTweetRep()
    {
      Linq2TwitterRepository
      (
        consumerKey           = s.ConsumerKey,
        consumerSecret        = s.ConsumerSecret,
        accessToken           = s.AccessToken,
        accessTokenSecret     = s.AccessTokenSecret
      )
    }
    def repInfo = RepCtors
    (
      UserRepository, 
      _ => NoteRepository(s.BotConStr),
      _ => getTweetRep(),
      _ => ProfileRepository(s.BotConStr),
      b => JuickRepository(b, s.JuickJid)
    );
    bot = ChatBot(conInfo, roomInfo, repInfo);
    bot.OnMessage += BotHelper.ParseMessage;
    bot.OnError += (_,e) => WriteLine(e.Message);
    
  }
  public Start() : void
  {
    bot.Connect();
  }
  
  public Stop() : void
  {
    bot.Disconect();
  }
}

