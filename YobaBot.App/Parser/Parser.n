﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Peg;

using System;
using System.Collections.Generic;
using System.Linq;


namespace YobaBot.App {
    type nt = NToken;
    type ut = YobaBot.Bot.UserType;

    [PegGrammar(Option = EmitDebugSources, tokens, grammar {
        s                  : void            = [Zs] / '\t' / '\v' / '\f';
        d                  : int             = ['0'..'9']+;
        word               : string          = (!s [Any])+;
        nameChars                            = [Any];
        ставь                                = ("по")? "став";
        гони                                 = "гони" [Any]? [Any]?;
        это                                  = ("эт" [Any]? [Any]? [Any]?) / "this" / "that";
        хек                : void            = (ставь / это / гони / "где" / "мой");
        имя                : string          = (s* (!s !"для" !лойс !зашквор !защитан !слив !хек nameChars)+)+;
        покажи             : void            = "покажи" s+;
        список             : void            = "список" s+;
        заметку            : void            = "заметку" s*;
        заметки            : void            = "заметки" s+;
        @:                 : void            = ':';
        //заметок            : void            = "заметок";
        защитан            : void            = "защитан" / "засчитан";
        строку             : void            = "строку" s+;
        добавь             : void            = ("добавь" / "создай") s+;
        удали              : void            = "удали" s+;
        обнови             : void            = "обнови" s+;
        из                 : void            = "из" s+;
        в                  : void            = "в" s+;
        для                : void            = "для";
        атрибут            : void            = "атрибут" / "аттрибут" / "параметер" / "параметр" / "значение" / "инфо";
        жид                : void            = ("жид" / "jid" / "jabberid") s+;
        твитни             : void            = "твитни";
        транслитом         : void            = "транслитом" s+;
        профиль            : void            = "профиль" s+;
        лойс               : void            = "лойс" / "лимон" / "лаймон" / "апельсин" / "lois" / "лайк" / "нойс";
        слив               : void            = "слив";
        жуик               : void            = "жуик" / "жуйк" / "juick" / "juik";
        соц                : void            = "соц" / "soc";
        топ                : void            = "топ" s+;
        разбань            : void            = "разбань";
        создай             : void            = "создай" / "новый";
        установи           : void            = "установи" / "измени" / "задай";
        пользователей      : void            = "пользователей" s*;
        зашквор            : void            = "зашквор" / "зашквар" / "zashkvor" / "zashkvar";
        алиас              : void            = ("алиас" / "alias" / "псевдоним" / "погоняло" / "кличку" / "позывной");
        версия                               = "версия" / "-v" /  "/v";
        вангуй             : void            = "вангуй" / "гадай" / "предскажи";
        contentWord        : Content         = word;
        brOpen             : void            = '[';
        brClose            : void            = '[';
        sep                : void            = (',' s+) / (s+ "или" s+);
        http               : Content         = "http://" word; 
        https              : Content         = "https://" word;
        twitToken          : Content         = https / http / contentWord;
        botName            : void            = s* ("yoba" / "ёба" / "еба" / "йоба" / "ёбамысо" / ":yoba:") s+;
        ёба                : void            = botName;
        delNoteLine        : NoteAction      = ёба удали строку? d s+ из заметки? [Any]+ ;
        addToNote          : NoteAction      = ёба добавь? в заметку (!":" [Any])+ ":" [Any]+;
        updateNote         : NoteAction      = ёба обнови заметку (!":" [Any])+ ":" [Any]+;
        showNote           : NoteAction      = ёба покажи заметку [Any]+;
        listNotes          : NoteAction      = ёба покажи список "заметок" s*;
        note               : YobaResult      = delNoteLine / updateNote / showNote / listNotes / addToNote;
        listUsers          : UserAction      = ёба покажи список ("админов" / "овнеров" / "модеров" / "банов" / "мемберов" / "голосов");
        unbanUser          : UserAction      = ёба разбань s+ [Any]+;
        user               : YobaResult      = listUsers / unbanUser;
        postTwit           : YobaResult      = ёба твитни (s+ twitToken)+ s*;
        postJuick          : YobaResult      = ёба жуик s+ [Any]+;
        postSoc            : YobaResult      = ёба соц (s+ twitToken)+ s*;
        translit           : YobaResult      = ёба транслитом [Any]+;
        help               : YobaResult      = ёба ("help" / "помощь" / "справка" / "?" / "-h" / "/h");
        addProfile         : ProfileAction   = ёба добавь профиль имя s*;
        addProfileAlias    : ProfileAction   = ёба добавь? алиас имя s+ для s+ имя s*;
        addProfileJid      : ProfileAction   = ёба добавь? жид word s+ для s+ имя s*;
        addProfileLois     : ProfileAction   = (ёба? лойс s+ имя) / (имя s+ лойс) (s+ защитан)? s*;
        addProfileZashkvor : ProfileAction   = (ёба? зашквор s+ имя) / (имя s+ зашквор) (s+ защитан)? s*;
        addProfileSliv     : ProfileAction   = ((ёба? слив s+ имя) / (имя s+ слив)) s+ защитан s*;
        addProfileAttr     : ProfileAction   = ёба атрибут s+ word s+ для s+ (! @: [Any])+ s* @: [Any]+;
        newProfileAttr     : ProfileAction   = ёба создай s+ атрибут s+ word;
        showProfileAttr    : ProfileAction   = ёба покажи атрибут s+ word;
        listProfileAttrs   : ProfileAction   = ёба покажи список "атрибутов";
        showTop            : ProfileAction   = ёба покажи топ '-'? d (s+ пользователей)? s*;
        showProfile        : ProfileAction   = ёба покажи профиль имя s*;
        version            : YobaResult      = ёба версия;
        ping               : YobaResult      = ёба? s* ("ping" / "p" / "п") s*;
        vangaQuestion      : YobaResult      = ёба вангуй s+ [Any]+;
        vangaOr            : YobaResult      = ёба вангуй s+ (!sep [Any])+ (sep (!sep [Any])+)+;
        vangaQuestionOr    : YobaResult      = ёба вангуй s+ (! @: [Any])+ @: (!sep [Any])+ (sep (!sep [Any])+)+;
        vanga              : YobaResult      = vangaQuestionOr / vangaOr / vangaQuestion;
        profile            : YobaResult      = addProfile / addProfileAlias / addProfileJid 
                                             / addProfileLois / addProfileZashkvor / addProfileSliv / showTop 
                                             / showProfile / addProfileAttr / newProfileAttr / showProfileAttr 
                                             / listProfileAttrs;
        tokens             : YobaResult      = (translit / note / vanga / profile / user / postTwit / postJuick / postSoc 
                                             / help / ping / version) ![Any];
    })]
    class Parser {
        clearTxt : nt -> string = t => GetText(t).Trim();
        clearQ   : nt -> string = t => GetText(t).Trim().TrimEnd(array['?']);
        d(tok : nt) : int { tok |> GetText |> int.Parse }
        word(word: nt) : string { word |> GetText  }
        contentWord(word : string)  : Content { word |> Content.Word }
        http(_ : nt, uri : string)  : Content { $"http://$uri"  |> Uri.Http  |> Content.Link }
        https(_ : nt, uri : string) : Content { $"https://$uri" |> Uri.Https |> Content.Link }
        имя(ts : List[nt]) : string { string.Join(" ", ts.Select(x => GetText(x).Trim())) }
        listUsers(roleTok : nt) : UserAction {
            def roleText = GetText(roleTok).ToLowerInvariant();
            def role = match(roleText) {
                              | "админов"  => ut.Admin();
                              | "овнеров"  => ut.Owner();
                              | "модеров"  => ut.Moderator();
                              | "банов"    => ut.Ban();
                              | "мемберов" => ut.Member();
                              | "голосов"  => ut.Voice();
                              | _          => throw ArgumentException($"Не сущесвует роли для $roleText")
                          }
            role |> UserAction.List
        }
        version(_ : nt) : YobaResult { YobaResult.Version() }
        unbanUser(tok : nt) : UserAction { GetText(tok).Trim() |> UserAction.Unban }
        user(act : UserAction) : YobaResult { act |> YobaResult.User}
        postTwit(tokens : List[Content]) : YobaResult { tokens.NToList() |> TwitAction.Post |> YobaResult.Twit(_, false) }
        postJuick(t : nt) : YobaResult { t |> clearTxt |> YobaResult.Juick } 
        postSoc(tokens : List[Content]) : YobaResult { tokens.NToList() |> TwitAction.Post |> YobaResult.Twit(_, true) }
        updateNote(note : nt, _ : nt, content : nt) : NoteAction { NoteAction.Update(note |> GetText, content |> GetText)}
        delNoteLine(line : int, note : nt) : NoteAction {  NoteAction.DelLine(note |> GetText, line) }
        listNotes(_ : nt) : NoteAction { NoteAction.List() }
        addToNote(note : nt, _ : nt, content : nt) : NoteAction { NoteAction.Add(note |> clearTxt, content |> clearTxt)}
        showNote(name : nt) : NoteAction { name |> GetText |> NoteAction.Show  }
        note(action : NoteAction) : YobaResult { action |> YobaResult.Note }
        translit(text : nt) : YobaResult { text |> GetText |> YobaResult.Translit }
        help(_ : nt ) : YobaResult { YobaResult.Help() }
        addProfile(name : string) : ProfileAction { name |>  ProfileAction.Add }
        addProfileAlias(alias : string, name : string) : ProfileAction { ProfileAction.AddAlias(name, alias) }
        addProfileJid(jid : string, name : string) : ProfileAction { ProfileAction.AddJid(name, jid) }
        addProfileLois(name : string) : ProfileAction { name |> ProfileAction.AddLois }
        addProfileZashkvor(name : string ) : ProfileAction { name |> ProfileAction.AddZashkvor }
        addProfileSliv(name : string) : ProfileAction { name |> ProfileAction.AddSliv }
        addProfileAttr(attr : string, name : nt, content : nt) : ProfileAction { ProfileAction.AddAttr(name |> GetText, attr, content |> GetText); }
        newProfileAttr(attr : string) : ProfileAction { ProfileAction.NewAttr(attr) }
        showProfileAttr(attr : string) : ProfileAction { ProfileAction.ShowAttr(attr) }
        showTop(minus : nt, count : int) : ProfileAction { ProfileAction.ShowTop(count, !minus.IsEmpty) }
        listProfileAttrs( _ : nt) : ProfileAction { ProfileAction.ListAttrs() }
        showProfile(name : string) : ProfileAction { name |> ProfileAction.ShowProfile }
        profile(action : ProfileAction) : YobaResult { action |> YobaResult.Profile }
        vangaQuestion(q : nt) : YobaResult { YobaResult.Vanga(q |> clearQ, List()) }
        vangaOr(q : nt, qs : List[nt]) : YobaResult { qs.Add(q); YobaResult.Vanga(null, qs.Select(clearQ).ToList()) }
        vangaQuestionOr(q : nt, v : nt, vs : List[nt]) : YobaResult { vs.Add(v); YobaResult.Vanga(q |> clearQ, vs.Select(clearQ).ToList()) }
        ping(r : nt) : YobaResult { GetText(r).Trim().ToLowerInvariant() |> YobaResult.Ping }
    }

    public variant YobaResult {
        | Twit       { action : TwitAction; soc : bool = true }
        | User       { action : UserAction      }  
        | Note       { action : NoteAction      }
        | Translit   { text   : string          }
        | Profile    { action : ProfileAction   }
        | Help
        | Ping       { requst : string          }
        | Vanga      { question : string; cases : IList[string] }
        | Juick      { message : string         }
        | Version
    }
    public variant UserAction {
        | List       { role : YobaBot.Bot.UserType }
        | Unban      { jid  : string }
    }
    public variant NoteAction { 
        | Add        { note : string; content : string }
        | Update     { note : string; content : string }
        | DelLine    { note : string; line : int }
        | List      
        | Show       { note : string }
    }
    public variant ProfileAction {
        | Add         { name  : string }
        | AddAlias    { name  : string; alias : string }
        | AddJid      { name  : string; jid : string }
        | AddLois     { name  : string }
        | AddZashkvor { name  : string }
        | AddSliv     { name  : string }
        | ShowTop     { count : int; bottom : bool }
        | ShowProfile { name  : string }
        | AddAttr     { name  : string; attr : string; content : string }
        | NewAttr     { attr  : string }
        | ShowAttr    { attr  : string }
        | ListAttrs
    }
    public variant TwitAction {
        | Post      { content: list[Content] }
    }
    public variant Uri {
        | Http  { uri : string }
        | Https { uri : string } 
    }
    public variant Content {
        | Word  { word : string }
        | Link  { uri  : Uri    }
        public override ToString() : string {
            match(this) {
                | Content.Word(s)            => s;
                | Content.Link(Uri.Http(l))  => l;
                | Content.Link(Uri.Https(l)) => l;
            }
        }
    }
}
