﻿using Nemerle;
//using Nemerle.Collections;
using Nemerle.Imperative;
using Nemerle.Peg;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using YobaBot.Bot;
using agsXMPP.protocol.client;
using agsXMPP;
using System.Threading.Tasks;
using BinaryAnalysis.UnidecodeSharp;


namespace YobaBot.App {

    public module BotHelper {
        Rnd : Random = Random();
        VangaAnswers = array[ "да", "нет", "это не важно", "спок, бро", "толсто", 
                              "да, хотя зря", "никогда", "100%", "1 из 100"];
        #region util
        public requireMsgType(ty : MessageType, m: Message, errMsg = ""): void{
            when(ty != m.Type) {
                def errText = if(!string.IsNullOrEmpty(errMsg)) errMsg
                                 else $"Необходим тип сообщения $ty, получен $(m.Type)";
                throw ArgumentException(errText);
            }
        }
        public requireRealJid(mi : MsgInfo, errMsg = ""): void{
            when(mi.From.IsNone) {
                def errText = 
                  if(!string.IsNullOrEmpty(errMsg)) 
                      errMsg
                  else {
                      def nick = mi.data.From?.Resource?.ToString();
                      $"$nick, участники, чей настоящий ЖИД не известен, не могут влиять на кармочку";
                  }
                throw ArgumentException(errText);
            }
        }
        public requireAdmin(mi : MsgInfo, errMsg = "") : void {
            match(mi.From) {
                | Some(f) when f.IsAdmin => ();
                | _                      =>
                    def errText = if(!string.IsNullOrEmpty(errMsg)) errMsg
                                else "Только администраторы могут разбанивать";
                    throw InvalidOperationException(errText);
            }
        }
        answerOrSilence(cond: bool, txt: string) : BotAnswer {
            if(cond) BotAnswer.Text(txt) else silence;
        }
        answer  : string -> BotAnswer  = BotAnswer.Text;
        ok      : BotAnswer            = BotAnswer.Default();
        silence : BotAnswer            = BotAnswer.Nothing();
        #endregion
        public TryParse(text: string) : option[YobaResult] {
            if(text == null) None()
            else {
                def parser = Parser();
                def parseData = SourceSnapshot(text).WithText(text.ToLowerInvariant());
                def (pos, data) = parser.TryParse(parseData);
                if(pos == text.Length) Some(data) else None()
            }
        }
        public ParseMessage(_ : object, mi : MsgInfo) : void {
            def bot = mi.bot;
            when(bot.JuickRep.IsJuickMsg(mi)) {
                bot.Send(mi.data.Body, MessageType.groupchat);
                return;
            }
            when(TryParse(mi.data.Body) is Some(v)) {
                def reply = 
                    try match(v) {
                        | Twit(TwitAction.Post as act, true)    => Soc(mi, act)
                        | Twit(act, _)                          => Twit(mi, act)
                        | Juick(msg)                            => Juick(bot, msg)
                        | User(act)                             => User(mi, act)
                        | Note(act)                             => Note(mi, act)
                        | Profile(act)                          => Profile(mi, act)
                        | Translit as t                         => Translit(mi, t)
                        | Help     as h                         => Help(mi, h)
                        | Ping(r)                               => 
                            if(mi.From.Value.ChatNick != mi.bot.BotNick)
                                answer(if(r == "1") r else "pong")
                            else 
                                silence;
                        | Vanga as rq                           => Vanga(mi, rq);
                        | Version                               =>
                            System.Reflection.Assembly
                            .GetExecutingAssembly()
                            .GetName().Version.ToString()
                            |> answer
                    }
                    catch { 
                        | e  => $"Ошибка: $(e.Message)" |> answer
                    }
                when(reply is BotAnswer.Nothing == false)
                    bot.Response(mi.data, reply.ToString());
            }
        }

        Juick(bot : ChatBot, msg : string) : BotAnswer {
            bot.JuickRep.Post(msg); silence
        }

        Twit(mi : MsgInfo, act : TwitAction) : BotAnswer {
            def twitRep = mi.bot.TwitRep;
            match(act) {
                | TwitAction.Post as t => twitRep.PostTwit(t) |> answer;
            }
        }

        Soc(mi : MsgInfo, act : TwitAction.Post) : BotAnswer {
            def toJuick(tokens) {
                def isTag   = _.StartsWith("#");
                def untag   = _.TrimStart(array['#']);
                def words   = tokens.Select(_.ToString());
                def tags    = words.Where(isTag).Select(s => "*" + untag(s));
                def tokens' = words.Reverse().SkipWhile(isTag).Reverse().Select(untag);
                string.Join(" ", tags.Concat(tokens'))
            }
            def juickMsg = toJuick(act.content);
            def answer   = Twit(mi, act);
            _ = Task.Run(() => mi.bot.JuickRep.Post(juickMsg));
            answer
        }
        Translit(_ : MsgInfo, trInfo : YobaResult.Translit) : BotAnswer {
            $<#"$(trInfo.text.Trim().Unidecode())"#> |> answer
        }
        Help(mi : MsgInfo, _ : YobaResult.Help) : BotAnswer {
            if(Settings.Default.HelpFile |> File.Exists)
                Settings.Default.HelpFile |> File.ReadAllText |> answer
            else 
                Note(mi, "ёба справка" |> NoteAction.Show);
        }
        Vanga(mi : MsgInfo, rq : YobaResult.Vanga) : BotAnswer {
            def prediction = match(rq){
                | (null, qs)             => qs[Rnd.Next(0, qs.Count)];
                | (_, qs) when !qs.Any() => VangaAnswers[Rnd.Next(0, VangaAnswers.Length)];
                | (_, qs)                => qs[Rnd.Next(0, qs.Count)];
            }
            def client = mi.From.Map(x => x.ChatNick + ",").WithDefault("");
            answer(if(rq.question == null) $<# $client "$prediction" #> 
                   else $<# $client "$(rq.question)" - $prediction#>);
        }
        Profile(mi : MsgInfo, action : ProfileAction) : BotAnswer {
            def profileRep = mi.bot.ProfRep;
            
            def tryByNameThenByJid(name, f) {
                def userInfo = mi.bot.UserRep.GetUserInfo(mi.bot.RoomJid + "/" + name);
                try {
                    def profile = profileRep.GetProfile(name);
                    if(userInfo is Some(x) when profile.isTransport)
                    {
                      def user = profileRep.GetProfileByJid(x.Resource);
                      f(user.name);
                    }
                    else 
                      f(name)
                }
                catch {
                    | _ is  System.Data.ObjectNotFoundException =>
                        match(userInfo) {
                            | Some(jid) => f(profileRep.GetProfileByJid(jid.RealJid).name) 
                            | _         => f(name);
                        }
                }
            }
            match(action) {
                | ProfileAction.Add(name)               => profileRep.Add(name); ok
                | ProfileAction.AddAlias(name, alias)   => profileRep.AddAlias(name, alias); ok
                | ProfileAction.AddJid(name, jid)       => profileRep.AddJid(name, jid); ok
                | ProfileAction.AddLois(name)           => 
                    requireMsgType(MessageType.groupchat, mi.data, "Лойсы можно ставить только в груповом чате");
                    requireRealJid(mi);
                    tryByNameThenByJid(
                        name,
                        n => answerOrSilence(profileRep.AddLois(mi.From.Value, n),"ЛОЙС поставлен"))
                | ProfileAction.AddZashkvor(name)       => 
                    requireMsgType(MessageType.groupchat, mi.data, "Зашкворы можно защитывать только в груповом чате");
                    requireRealJid(mi);
                    tryByNameThenByJid(
                        name,
                        n => answerOrSilence(profileRep.AddZashkvor(mi.From.Value, n), "Зашквор засчитан")); 
                | ProfileAction.AddSliv(name)           =>
                    requireMsgType(MessageType.groupchat, mi.data, "Сливы можно защитывать только в груповом чате");
                    def nick = mi.data.From?.Resource?.ToString();
                    requireRealJid(mi,  $"$nick, участники, чей настоящий ЖИД не известен, не могут засчитывать сливы");
                    tryByNameThenByJid(
                        name, 
                        n => answerOrSilence(profileRep.AddSliv(mi.From.Value, n), "Слив защитан"));
                | ProfileAction.ShowTop(count, bottom)  => 
                    def profs = profileRep.GetTop(count, bottom);
                    def nprofs = profs.MapI((i,p) => $"$(i+1). $p");
                    string.Join(Environment.NewLine, nprofs) |> answer
                | ProfileAction.ShowProfile(name)       => 
                    tryByNameThenByJid(name, n => profileRep.GetProfile(n).ToString() |> answer);
                | ProfileAction.ListAttrs =>
                    def attrs = profileRep.ListAttrs();
                    def s = if(attrs.Any()) profileRep.ListAttrs().ToString() else "Их нет";
                    s |> answer;
                | ProfileAction.NewAttr(attr) =>
                    requireRealJid(mi, "Участники, чей настоящий ЖИД не известен, не могут создавать атрибуты для профилей");
                    when(!mi.From.Value.IsOwner)
                        throw Security.SecurityException("Только овнеры могут создавать атрибуты");
                     _ = profileRep.NewAttr(attr); ok;
                | ProfileAction.AddAttr(name, attr, val) =>
                    requireRealJid(mi, "Участники, чей настоящий ЖИД не известен, не могут изменять атрибуты пользователей");
                    def jid = mi.bot.UserRep.GetUserInfo(mi.bot.RoomJid + "/" + name).Map(_.RealJid).WithDefault("pok pok");
                    _ = profileRep.AddAttr(attr, val, name, jid); ok;
                | ProfileAction.ShowAttr(attr) =>
                    def atrs = profileRep.ShowAttr(attr);
                    def s = if(atrs.Any()) Environment.NewLine + string.Join(Environment.NewLine, atrs) 
                            else "Нетути ничегошечки";
                    s |> answer;
            }
        }
        Note(mi: MsgInfo, action : NoteAction) : BotAnswer{
            def NoteRep = mi.bot.NoteRep;
            match(action) {
                | NoteAction.Add(n,c)      => NoteRep.InsertContent(n, c); ok
                | NoteAction.DelLine as d  => NoteRep.DeleteNoteLine(d.note, d.line); ok
                | NoteAction.Update(n,c)   => NoteRep.UpdateNote(n, c); ok
                | NoteAction.List          =>  
                    def notes = NoteRep.ListNotes().Select(n => $"$(n.number). $(n.name)");
                    string.Join(Environment.NewLine, notes) |> answer
                | NoteAction.Show as s     => 
                    def note = NoteRep.GetNote(s.note);
                    def numLines = note.content.Map((i,l) => $"$i. $l"); 
                    def content = string.Join(Environment.NewLine, numLines);
                    $"$(note.name)$(Environment.NewLine)$content" |> answer
            }
        }
        User(mi: MsgInfo, action : UserAction) : BotAnswer {
            def userRep = mi.bot.UserRep;
            match(action) {
                | UserAction.List as l => 
                    def lst = userRep.GetList(l.role);
                    def result = if(lst.Any()) {
                        def indexedlst =lst.Select((j, n) => $"$(n+1). $j");
                        string.Join(", ", indexedlst)
                    }
                    else "нетути никогошеньки";
                    result |> answer
                | UserAction.Unban(jid) => 
                    requireAdmin(mi);
                    mi.bot.UserRep.RemoveRoles(Jid <| jid);
                    ok
            }
        }
    }
}