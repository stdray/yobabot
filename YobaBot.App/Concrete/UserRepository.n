﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Linq;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using System.Xml.XPath;

using agsXMPP;
using agsXMPP.protocol.x.muc;
using agsXMPP.protocol.client;
using YobaBot.Bot;

namespace YobaBot.App {
    public class UserRepository : IUserRepository {
        static xName = XName.Get("x", "http://jabber.org/protocol/muc#user");
        public Bot : ChatBot;

        UserMap : ConcurrentDictionary[string, UserInfo] = ConcurrentDictionary();

        public this(bot : ChatBot) {
            Bot = bot;
            Bot.OnPresence += onPresense;
        }

        getUserInfo(text : string) : option[UserInfo] {
            def getAttribute(item, attrName) { item.Attribute(attrName)?.Value; }
            def xml = XElement.Parse(text);
            def nick = getAttribute(xml, "from");
            def items = linq <# from _x in xml.Descendants(xName)
                                from item in _x.Descendants()
                                where item.Attributes("jid").Any()
                                select item #>;
            def item = items.FirstOrDefault();
            if(item == null) None()
            else {
                def getAttr = getAttribute(item, _);
                def jid = getAttr("jid");
                def affiliation = getAttr("affiliation");
                def role = getAttr("role");
                UserInfo(nick, jid, affiliation, role) |> Some
            }
        }

        onPresense(_ : object, p : PresenceInfo) : void {
            match(p.data.ToString() |> getUserInfo) {
                | None    => ();
                | Some(u) => _ = UserMap.AddOrUpdate(u.FullChatJid |> toKey, u, (_, _) => u);
            }
        }

        toKey : string -> string = s => s.ToLowerInvariant().Trim(':');

        public GetRealJid(nick : string) : option[string] {
           match(getUserInfo <| nick) {
               | Some(ui) => ui.RealJid |> Some
               | None     => None()
           }
        }

        public GetList(role : YobaBot.Bot.UserType) : list[string] {
            def wl = WaitList();
            match(role) {
                | UserType.Admin     => Bot.Conference.RequestAdminList(Bot.RoomJid, wl.Callback, null);
                | UserType.Owner     => Bot.Conference.RequestOwnerList(Bot.RoomJid, wl.Callback, null);
                | UserType.Moderator => Bot.Conference.RequestModeratorList(Bot.RoomJid, wl.Callback, null);
                | UserType.Member    => Bot.Conference.RequestMemberList(Bot.RoomJid, wl.Callback, null);
                | UserType.Voice     => Bot.Conference.RequestVoiceList(Bot.RoomJid, wl.Callback, null);
                | UserType.Ban       => Bot.Conference.RequestBanList(Bot.RoomJid, wl.Callback, null);
            }
            wl.UsersList;
        }

        public RemoveRoles(jid : Jid) : void {
            Bot.Conference.ModifyList(Bot.RoomJid, array[RoleItem(ChatRole.none, jid)]);
        }

        public SetRole(role : YobaBot.Bot.UserType, jid : Jid, reason : string = null) : void {
            def setRole(role, jid){
                Bot.Conference.ModifyList(Bot.RoomJid, array[RoleItem(role, jid)]);
            }
            match(role) {
                | UserType.Admin     => setRole(ChatRole.admin, jid);
                | UserType.Owner     => setRole(ChatRole.owner, jid);
                | UserType.Member    => setRole(ChatRole.member, jid);
                | UserType.Ban       => Bot.Conference.BanUser(Bot.RoomJid, jid, reason);
                | UserType.Voice     => Bot.Conference.GrantVoice(Bot.RoomJid, jid.User,reason);
                | UserType.Moderator => ()
            }
        }
        public GetUserInfo(nick : string) : option[YobaBot.App.UserInfo] {
            mutable userInfo;
            if(UserMap.TryGetValue(nick |> toKey, out userInfo)) {
                userInfo |> Some
            }
            else None();
        }
        
    }

    public class WaitList {
        mutable listEvent : AutoResetEvent = AutoResetEvent(false);
        mutable _users : list[string];
        public Callback(_ : object, iq : IQ, _ : object) : void {
            _users = XElement.Parse(iq.InnerXml).Elements()
                            .Where(e => e.HasAttributes && e.Attribute("jid") != null)
                            .Select(e => e.Attribute("jid").Value)
                            .OrderBy(s => s).NToList();
            _ = listEvent.Set();
        }
        public UsersList : list[string] {
            get {
                _ = listEvent.WaitOne();
                _users;
            }
        }
    }

    [Record] public class UserInfo {
        public FullChatJid  : string;
        public FullRealJid  : string;
        public affiliation  : string;
        public role         : string;
        public ChatNick     : string { get { FullChatJid.Split(array['/']).Last(); } }
        public RealJid      : string { get { FullRealJid.Split(array['/']).First(); } }
        public Resource     : string { get { FullRealJid.Split(array['/']).Last(); } }
        public UserRole     : UserType { 
            get {
                match(affiliation.ToLowerInvariant()){
                    | "owner"  => UserType.Owner();
                    | "admin"  => UserType.Admin();
                    | "member" => UserType.Member();
                    | _        => UserType.Member();
                }
            }
        }
        public IsAdmin : bool {
            get{
                match(UserRole) {
                    | UserType.Admin | UserType.Owner => true
                    | _                               => false
                }
            }
        }
        public IsOwner : bool {
            get{
                match(UserRole) {
                    | UserType.Owner => true
                    | _                               => false
                }
            }
        }
        
    }
}
    