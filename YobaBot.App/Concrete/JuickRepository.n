﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using YobaBot.Bot;

namespace YobaBot.App
{
    public class JuickRepository : IJuickRepository
    {
        public Bot      : ChatBot;
        public JuickJid : agsXMPP.Jid;

        public this(bot : ChatBot, jid : string){
            Bot = bot;
            JuickJid = agsXMPP.Jid(jid);
        }

        public IsJuickMsg(msg : MsgInfo) : bool {
            def from = msg.data.From;
            from.Server == JuickJid.Server && from.User == JuickJid.User;
        }

        public Post(msg : string) : void {
            Bot.SendTo(JuickJid, msg)
        }
    }
}
