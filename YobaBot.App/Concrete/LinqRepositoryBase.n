﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using YobaBot.Db;

namespace YobaBot.App {
    [Record] class LinqRepositoryBase {
        protected _conStr : string;
        protected getDb() : HranilisheDataContext {  
            HranilisheDataContext(_conStr); 
        }
        protected save(act : HranilisheDataContext->void) : void{
            using(db = getDb()) {
                act(db);
                db.SubmitChanges();
            }
        }
        protected save[T](act : HranilisheDataContext->T) : T {
            using(db = getDb()) {
                def f = act(db);
                db.SubmitChanges();
                f
            }
        }
        protected read[T](act : HranilisheDataContext->T) : T {
            using(db = getDb())
                act(db);
        }
    }
}
