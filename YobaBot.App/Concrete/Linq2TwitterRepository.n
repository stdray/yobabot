﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using LinqToTwitter;
using NBoxx.Common.Patterns;

namespace YobaBot.App {
    [Record(Exclude = [settings])]
    public class Linq2TwitterRepository : ITwitterRepository {
        class TwitterSettings {
            public short_url_length       : int {get;set}
            public short_url_length_https : int {get;set;}
        }
        credentials : IOAuthCredentials;
        settings : TimeCache[TwitterSettings];
        public this (consumerKey : string, consumerSecret : string,
                     accessToken : string, accessTokenSecret : string) {
            credentials = InMemoryCredentials() <- {
                AccessToken     = accessTokenSecret;
                ConsumerKey     = consumerKey;
                ConsumerSecret  = consumerSecret;
                OAuthToken      = accessToken;
            };
            settings = TimeCache(TimeSpan.FromDays(1), getSettings);
        }

        public withTwitter[T](action: TwitterContext -> T) : T {
            def authorizer = SingleUserAuthorizer() <- {
                Credentials = credentials
            };
            using(def twitter = TwitterContext(authorizer))
                action(twitter);
        }
        getSettings() : TwitterSettings {
            withTwitter(twitter => {
                def request = Request("https://api.twitter.com/1.1/help/configuration.json");
                using(response = twitter.AuthorizedClient.Get(request).GetResponse()) {
                    def stream   = response.GetResponseStream();
                    def reader   = System.IO.StreamReader(stream);
                    def jsonStr  = reader.ReadToEnd();
                    def settings : TwitterSettings = JsonConvert.DeserializeObject(jsonStr);
                    settings
                }});
        }
        public PostTwit(postInfo : YobaBot.App.TwitAction.Post) : string {
            match(makeTweets <| postInfo.content) {
                | [t] => 
                    withTwitter(tw => t |> tw.UpdateStatus |> tweetLink);
                | ts  => 
                    def tweets = ts.Map(t => $"$(t.Length) [$t]");
                    string.Format("Твит слишком большой, рекомендуется разбить: {0} {1}",
                                  Environment.NewLine,
                                  string.Join(Environment.NewLine, tweets))
            }
        }
        tweetLink(status : Status) : string {
            $"https://twitter.com/zxc_by/status/$(status.StatusID)";
        }
        tokenLen(content : Content) : int {
            | Content.Word(s)         => s.Length;
            | Content.Link(Uri.Http)  => settings.Value.short_url_length;
            | Content.Link(Uri.Https) => settings.Value.short_url_length_https;
        }
        toTwit(tokens : list[Content]) : string {
            string.Join(" ", tokens)
        }
        makeTweets(tokens : list[Content]) : list[string]{
            def split(twits: List[string], cur, len, ts){
                match(ts, cur) {
                    | ([], [])                                     => twits;
                    | ([], t)                                      => twits.Add(toTwit(t)); twits;
                    | (x::_, _) when tokenLen(x) > 140             => throw ArgumentException($"Невозможно корректно разбить сообщение на части по 140 символов. Разбейте пробелами: $x)");
                    | (x::xs, [])                                  => split(twits, [x], tokenLen(x), xs);
                    | (x::xs, cs) when len + tokenLen(x) + 1 <=140 => split(twits, cs + [x], len + tokenLen(x) + 1, xs);
                    | (_, cs)                                      => twits.Add(toTwit(cs)); split(twits, [], 0, ts);
                }
            }
            split(List.[string](), [], 0, tokens).NToList()
        }

    }
}