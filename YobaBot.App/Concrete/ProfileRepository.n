﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using YobaBot.Db;

namespace YobaBot.App {
    [Record] 
    class ProfileRepository : LinqRepositoryBase, IProfileRepository {
        static Ban      : string = "Вас лишили права влиять на лойсо-зашкварные характеристики участников";
        static SelfDeny : string = "Нельзя ставить лойсы самому себе";
        
        public Add(name : string) : void {
            def profile = Profile() <- {
                Id = Guid.NewGuid();
                Loisy = 0;
                Zashkvory = 0;
                MainName = name;
                CanVote = true;
                IsTransport = false;
            }
            save(db => {
                when(getProfile(db, name) is Some(p)) 
                    throw ArgumentException($"Имя $name уже используется для профиля $(p.MainName)");
                db.Profiles.InsertOnSubmit(profile);
            });
        }
        public AddAlias(name : string, alias : string) : void {
            save(db => {
                def prof = getProfileVal(db, name);
                when(getProfile(db, alias) is Some(p)) 
                     throw ArgumentException($"Имя $alias уже используется для профиля $(p.MainName)");
                prof.ProfileNames.Add(ProfileName() <- { 
                    Name = alias; 
                    ProfileId = prof.Id
                });
            })
        }
        public AddJid(name : string, jid : string) : void {
           save(db => {
               def prof = getProfileVal(db, name);
               when(getProfileByJid(db, jid) is Some(p))
                   throw ArgumentException($"Jid $jid уже используется для профиля $(p.MainName)");
               prof.ProfileJids.Add(ProfileJid() <- {
                   Jid = jid;
                   ProfileId = prof.Id;
               });
           })
        }
        public AddLois(jid: UserInfo, name : string) : bool {
            changeProfile(jid, name, p => p.Loisy++, 
                "Ваш jabberid не ассоциирован ни с одним профилем. Вы не можете ставить лойсы",
                SelfDeny,
                Ban)
        }
        public AddZashkvor(jid: UserInfo, name : string) : bool {
            changeProfile(jid, name, p => p.Zashkvory++, 
                "Ваш jabberid не ассоциирован ни с одним профилем. Вы не можете зашкваривать",
                SelfDeny,
                Ban)
        }
        public AddSliv(jid: UserInfo, name : string) : bool {
            changeProfile(jid, name, p => p.Slivi++, 
                "Ваш jabberid не ассоциирован ни с одним профилем. Вы не можете засчитывать сливы",
                SelfDeny,
                Ban)
        }
        public GetProfile(name : string) : ProfileItem {
            read(db => getProfileVal(db, name) |> makeProfileItem )
        }
        public GetProfileByJid(jid : string) : ProfileItem {
            read(db => 
                if(getProfileByJid(db, jid).Map(makeProfileItem) is Some(v)) v
                else throw ObjectNotFoundException($"Профиль $jid не найден"))
        }
        public GetTop(count : int, bottom : bool = false) : list[ListProfileItem] { 
            def karma(p) { 
                def k = Math.Sqrt((p.Loisy ** 2) + (p.Zashkvory **2)) / (100500 :> double);
                k * (p.Loisy - Math.Sqrt(0.93* (p.Zashkvory**2)))
            }
            read(db => {
                def profiles = 
                    if(bottom) db.Profiles.OrderBy(karma)
                    else       db.Profiles.OrderByDescending(karma);           
                profiles.Take(count).Map(makeListProfileItem)
            })
        }
        public NewAttr(attr : string) : bool {
            save(db =>
                if(db.Attributes.Any(a => a.Attribute1.Trim().ToLower() == attr.Trim().ToLower()))
                    false
                else {
                    def dbAttr = Attribute() <- { Attribute1 = attr; Id = Guid.NewGuid() };
                    db.Attributes.InsertOnSubmit(dbAttr);
                    true;
                } 
            )
        }
        public AddAttr(attr : string, val : string, name : string, jid : string) : bool{
            save(db => {
                def profile = getProfile(db, name);
                def profile = if(profile.IsSome) profile else getProfileByJid(db, jid);
                when(profile.IsNone)
                    throw ObjectNotFoundException($"Профиль $name не найден");
                def attr = db.Attributes.FirstOrDefault(a => a.Attribute1.Trim().ToLower() == attr.Trim().ToLower());
                when(attr == null)
                    throw ObjectNotFoundException($"Атрибут $attr не существует");
                def p = profile.Value;
                mutable pa = p.ProfileAttributes.FirstOrDefault(r => r.AttributeId == attr.Id);
                when(pa == null) {
                    pa = ProfileAttribute() <- { AttributeId = attr.Id; ProfileId = p.Id; };
                    db.ProfileAttributes.InsertOnSubmit(pa);
                }
                pa.Value = val;
                true;
            });
        }
        public ShowAttr(attr : string) : list[ProfileAttrListItem] {
            read(db => {
                def attr = db.Attributes.FirstOrDefault(a => a.Attribute1.Trim().ToLower() == attr.Trim().ToLower());
                when(attr == null)
                    throw ObjectNotFoundException($"Атрибут $attr не существует");
                attr.ProfileAttributes
                    .Select(pa => ProfileAttrListItem(pa.Profile.MainName, pa.Value))
                    .OrderBy(_.name)
                    .NToList()
            });
        }
        public ListAttrs() : list[string] {
            read(db => db.Attributes.Select(a => a.Attribute1).NToList());
        }
        changeProfile(jid: UserInfo, name : string, change : Profile -> void, 
                      reqJidTxt : string, denySelfChangeTxt : string, denyChangeTxt : string) : bool {
            save(db => {
              def from = match(getProfileByJid(db, jid.RealJid))
              {
                | None         => throw InvalidOperationException(reqJidTxt);
                | Some(p) as r => if(p.IsTransport) getProfile(db, jid.Resource) else r;
              };
              match(from, getProfile(db, name)) {
                  | (_, None)                                    => 
                      false;
                  | (None, _)                                    => 
                      throw InvalidOperationException(reqJidTxt);
                  | (Some(from), Some(to)) when from.Id == to.Id => 
                      throw InvalidOperationException(denySelfChangeTxt);
                  | (Some(from), _) when from.CanVote == false  => 
                      throw InvalidOperationException(denyChangeTxt);
                  | (_, Some(to)) => 
                      change(to); 
                      true;
              }
          })
        }
        getProfile(db : HranilisheDataContext, name : string) : option[Profile] {
            def lname = name.ToLowerInvariant();
            def profile = db.Profiles.SingleOrDefault(p => 
                p.MainName.ToLower() == lname ||
                p.ProfileNames.Any(pn => pn.Name.ToLower() == lname));
            if(profile == null) None() else profile |> Some;
        }
        getProfileByJid(db : HranilisheDataContext, jid : string) : option[Profile] {
            def ljid = jid.ToLowerInvariant();
            def profile = db.Profiles.SingleOrDefault(p => 
                p.ProfileJids.Any(j => j.Jid.ToLower() == ljid)
            );
            if(profile == null) None() else profile |> Some;
        }
        getProfileVal(db : HranilisheDataContext, name : string) : Profile {
             if(getProfile(db, name) is Some(p)) p
             else throw ObjectNotFoundException($"Профиль $name не найден");
        }
        makeListProfileItem(prof : Profile) : ListProfileItem{
            ListProfileItem(prof.MainName, prof.Loisy, prof.Zashkvory, prof.Slivi);
        }
        makeProfileItem(prof : Profile) : ProfileItem{
            def attrs = prof.ProfileAttributes
                .Select(pa => (pa.Attribute.Attribute1, pa.Value))
                .NToList();
            ProfileItem(prof.MainName, prof.Loisy, prof.Zashkvory, prof.Slivi,
                        prof.ProfileNames.Map(_.Name), 
                        prof.ProfileJids.Map(_.Jid),
                        prof.CanVote,
                        prof.IsTransport,
                        attrs);
        }
    }
}
