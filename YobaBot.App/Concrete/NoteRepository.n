﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using YobaBot.Db;

namespace YobaBot.App {

    [Record] class NoteRepository : LinqRepositoryBase, INoteRepository {
        public DeleteNoteLine(name : string, line : int) : void {
            save(db => {
                def note  = getNote(db, name);
                def lines = makeNumberedLines(note.Content).Where((i,_) => i != line);
                if(lines.Any()) 
                    note.Content = string.Join(Environment.NewLine, lines.Map(Pair.Second));
                else
                    db.Notes.DeleteOnSubmit(note);
            })
        }
        public GetNote(name : string) : YobaBot.App.NoteItem {
            read(db => {
                def note = getNote(db, name);
                def lines = makeNumberedLines(note.Content).NToList();
                YobaBot.App.NoteItem(note.DisplayNoteName, lines);
            })
        }
        public InsertContent(name : string, content : string) : void {
            inserOrUpdateNote(name, fun(note) {
                note.Content = if(note.Content == null) content
                               else note.Content + Environment.NewLine + content
            })
        }
        public ListNotes() : list[YobaBot.App.ListNoteItem] { 
            read(db => {
                def notes = db.Notes.OrderBy(_.Added).Select(_.DisplayNoteName).NToList();
                notes.MapI((i, n) => YobaBot.App.ListNoteItem(i+1, n));
            })
        }
        public UpdateNote(name : string, content : string) : void {
            inserOrUpdateNote(name, fun(note) {
                note.Content = content; 
                note.DisplayNoteName = name;
            });
        }
        getNote(db : HranilisheDataContext, noteName : string) : Note {
            def pkNoteName = makePkNoteName(noteName);
            def note = db.Notes.FirstOrDefault(n => n.NoteName == pkNoteName);
            when(note == null) 
                throw ArgumentException($"Заметка $noteName не найдена"); 
            note
        }
        inserOrUpdateNote(noteName : string, action : Note -> void) : void {
            def pkNoteName = makePkNoteName(noteName);
            save(db => {
                mutable note = db.Notes.FirstOrDefault(n => n.NoteName == pkNoteName);
                when(note == null) { 
                    note = Note() <- { 
                        Added = DateTime.Now; 
                        NoteName = pkNoteName;
                        DisplayNoteName = noteName;
                    } 
                    db.Notes.InsertOnSubmit(note)
                }
                note.Updated = DateTime.Now; 
                action(note);
            })
        }
        makePkNoteName(name : string) : string { name.Trim().ToLower().Replace(" ", "")  }
        makeLines(content : string) : array[string] { Regex.Split(content, "\r\n|\r|\n") }
        makeNumberedLines(content : string) : Seq[int * string] { makeLines(content).Select((l, i) => (i+1, l))  }
    }
}
