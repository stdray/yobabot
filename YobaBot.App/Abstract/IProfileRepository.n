﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace YobaBot.App {
    [Record] public class ListProfileItem {
        public name : string;
        public loicy : int;
        public zashkvory : int;
        public slivi : int;
        public override ToString() : string {
            $"$name [лойсы: $loicy, зашкворы: $zashkvory, сливы: $slivi]"
        }
    }

    [Record] 
    public class ProfileItem : ListProfileItem {
        public aliases     : list[string];
        public jids        : list[string];
        public canVote     : bool;
        public isTransport : bool;
        public attrs       : list[string * string];
        public override ToString() : string {
            def attrsMap = [("лойсы", loicy.ToString()),
                            ("зашкворы", zashkvory.ToString()),
                            ("сливы", slivi.ToString()),
                            ("имена", aliases.ToString()),
                            ("жиды", jids.ToString()),
                            ("может ставить лойсы", if(canVote) "да" else "нит")];
            def attrsMap = attrsMap.Concat(attrs.OrderBy((k, _) => k));
            def attrsStr = string.Join(Environment.NewLine, 
                                       attrsMap.Map((k, v) => $"$k : $v"));
            name + Environment.NewLine + attrsStr 
        }
    }
    
    [Record]
    public class ProfileAttrListItem {
        public name    : string;
        public attrVal : string;
        public override ToString() : string {
            $"$name: $attrVal"
        }
    }

    public interface IProfileRepository {
        Add(name : string) : void;
        AddAlias(name : string, alias : string) : void;
        AddJid(name : string, jid : string) : void;
        AddLois(jid: UserInfo, name : string) : bool;
        AddSliv(jid: UserInfo, name : string) : bool;
        AddZashkvor(jid: UserInfo, name : string) : bool;
        GetTop(count : int, bottom : bool = false) : list[ListProfileItem];
        GetProfile(name : string) : ProfileItem;
        GetProfileByJid(jid : string) : ProfileItem;
        NewAttr(attr : string) : bool;
        ShowAttr(attr : string) : list[ProfileAttrListItem];
        AddAttr(attr : string, val : string, name : string, jid : string) : bool;
        ListAttrs() : list[string];
    }
}
