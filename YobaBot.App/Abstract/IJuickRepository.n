﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using YobaBot.Bot;

namespace YobaBot.App
{
    public interface IJuickRepository
    {
        IsJuickMsg(msg : MsgInfo) : bool;
        Post(msg : string) : void;
    }
}
