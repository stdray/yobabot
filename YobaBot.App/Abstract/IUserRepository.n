﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using YobaBot.Bot;

namespace YobaBot.App{
    public interface IUserRepository {
        GetList(role : UserType) : list[string];
        RemoveRoles(jid : agsXMPP.Jid) : void;
        SetRole(role : UserType, jid : agsXMPP.Jid, reason : string = null) : void;
        GetRealJid(nick : string) : option[string];
        GetUserInfo(nick : string) : option[UserInfo];
    }
}
