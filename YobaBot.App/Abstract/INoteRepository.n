﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace YobaBot.App {
    [Record] public class ListNoteItem {
        public number : int;
        public name   : string;
    }

    [Record] public class NoteItem {
        public name     : string;
        public content  : list[int*string];
    }

    public interface INoteRepository {
        ListNotes() : list[ListNoteItem];
        GetNote(name : string) : NoteItem;
        DeleteNoteLine(name : string, line : int) : void;
        InsertContent(name : string, content : string) : void;
        UpdateNote(name : string, content : string) : void;
    }
}
