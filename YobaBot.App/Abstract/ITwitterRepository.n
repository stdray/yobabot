﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace YobaBot.App {
  public interface ITwitterRepository {
       PostTwit(postInfo : TwitAction.Post) : string ;
  }
}
