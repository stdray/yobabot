﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YobaBot.App;

namespace YobaBot.Tests {
    [TestClass]
    public class TestParser {
        [TestMethod]
        public void TestAddToList() {
            var r = BotHelper.TryParse("yoba в заметку рабочие столы: iop лока и дибил");
            Assert.IsTrue(r.IsSome);
            var data = r.Value;
            Assert.IsNotNull(data);
            Assert.IsTrue(data is YobaResult.Note);
            var note = data as YobaResult.Note;
            Assert.IsTrue(note.action is NoteAction.Add);
            var add = note.action as NoteAction.Add;
            Assert.AreEqual("рабочие столы", add.note);
            Assert.AreEqual("iop лока и дибил", add.content);
        }

        [TestMethod]
        public void TestShowList() {
            var r = BotHelper.TryParse("yoba покажи заметку рабочие столы");
            Assert.IsTrue(r.IsSome);
            var data = r.Value;
            Assert.IsNotNull(data);
            Assert.IsTrue(data is YobaResult.Note);
            var note = data as YobaResult.Note;
            Assert.IsTrue(note.action is NoteAction.Show);
            var add = note.action as NoteAction.Show;
            Assert.AreEqual("рабочие столы", add.note);
        }

        [TestMethod]
        public void TestShowUsers() {
            var r = BotHelper.TryParse("yoba покажи список модеров");
            Assert.IsTrue(r.IsSome);
            var data = r.Value;
            Assert.IsNotNull(data);
            Assert.IsTrue(data is YobaResult.User);
            var twit = data as YobaResult.User;
            Assert.IsTrue(twit.action is UserAction.List);
            var post = twit.action as UserAction.List;
            Assert.IsTrue(post.role is YobaBot.Bot.UserType.Moderator);
        }

        [TestMethod]
        public void TestPostTwit() {
            var r = BotHelper.TryParse("yoba твитни Всем Счастливого Нового года!");
            Assert.IsTrue(r.IsSome);
            var data = r.Value;
            Assert.IsNotNull(data);
            Assert.IsTrue(data is YobaResult.Twit);
            var twit = data as YobaResult.Twit;
            Assert.IsTrue(twit.action is TwitAction.Post);
            var post = twit.action as TwitAction.Post;
            //Assert.IsTrue(post.ro is YobaBot.Bot.UserType.Moderator);
        }
    }
}
