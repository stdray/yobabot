
/****** Object:  Table [dbo].[Attribute]    Script Date: 11/04/2013 02:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attribute](
	[Attribute] [nvarchar](200) NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProfileAttribute]    Script Date: 11/04/2013 02:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfileAttribute](
	[ProfileId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ProfileAttribute] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC,
	[AttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_ProfileAttribute_Attribute]    Script Date: 11/04/2013 02:54:02 ******/
ALTER TABLE [dbo].[ProfileAttribute]  WITH CHECK ADD  CONSTRAINT [FK_ProfileAttribute_Attribute] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[Attribute] ([Id])
GO
ALTER TABLE [dbo].[ProfileAttribute] CHECK CONSTRAINT [FK_ProfileAttribute_Attribute]
GO
/****** Object:  ForeignKey [FK_ProfileAttribute_Profile]    Script Date: 11/04/2013 02:54:02 ******/
ALTER TABLE [dbo].[ProfileAttribute]  WITH CHECK ADD  CONSTRAINT [FK_ProfileAttribute_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO
ALTER TABLE [dbo].[ProfileAttribute] CHECK CONSTRAINT [FK_ProfileAttribute_Profile]
GO
