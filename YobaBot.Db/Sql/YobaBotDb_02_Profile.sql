USE [YobaBot]
GO

/****** Object:  Table [dbo].[Profile]    Script Date: 02/27/2013 19:18:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

print 'Create table Profile'
GO

CREATE TABLE [dbo].[Profile](
	[Id] [uniqueidentifier] NOT NULL,
	[MainName] [nvarchar](300) NOT NULL,
	[Loisy] [int] NOT NULL,
	[Zashkvory] [int] NOT NULL,
 CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

print 'Create table ProfileName'
GO

CREATE TABLE [dbo].[ProfileName](
	[ProfileId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_ProfileName] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProfileName]  WITH CHECK ADD  CONSTRAINT [FK_ProfileName_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProfileName] CHECK CONSTRAINT [FK_ProfileName_Profile]
GO

print 'Create table ProfileJid'
GO

CREATE TABLE [dbo].[ProfileJid](
	[ProfileId] [uniqueidentifier] NOT NULL,
	[Jid] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_ProfileJid] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC,
	[Jid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProfileJid]  WITH CHECK ADD  CONSTRAINT [FK_ProfileJid_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProfileJid] CHECK CONSTRAINT [FK_ProfileJid_Profile]
GO